"use strict";

const React = require("react");

module.exports = function Layout({ children }) {
	return (
		<html lang="en">
		<head>
			<meta charSet="UTF-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			<meta httpEquiv="X-UA-Compatible" content="ie=edge"/>
			<link rel="stylesheet" href="/style.css"/>
			<title>Matrix Room List Viewer</title>
		</head>
		<body>
			<div className="container">
				<h1>Matrix Room List Viewer</h1>
				<a href="https://git.cryto.net/joepie91/matrix-room-list-viewer">Source code</a>
				<hr/>
				{children}
			</div>
		</body>
		</html>
	);
};
