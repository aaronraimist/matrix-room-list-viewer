"use strict";

const React = require("react");

const Layout = require("./layout");

module.exports = function Error({ error }) {
	return (
		<Layout>
			<pre>
				{error.message}
			</pre>
		</Layout>
	);
};
